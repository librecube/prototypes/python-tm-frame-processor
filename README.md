# Return Frame Processor

...

## Development Setup

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/prototypes/python-return-frame-processor
$ cd python-tm-frame-processor
$ python -m venv venv
$ source venv/bin/activate
$ pip install -e .
```

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-return-frame-processor/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-return-frame-processor

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
