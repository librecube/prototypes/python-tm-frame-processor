import os
import xmlrpc.client

import yaml
import click


# load main config file
config_file = os.environ.get("CONFIG_FILE", "config.yml")
if not os.path.exists(config_file):
    raise ValueError(f"The file {config_file} was not found.")
with open(config_file, 'r') as f:
    config = yaml.safe_load(f)

client = xmlrpc.client.ServerProxy(
    f"http://localhost:{config['controller']['port']}", allow_none=True)


@click.group()
def cli():
    pass


@cli.command()
def ping():
    click.echo(client.ping())


if __name__ == "__main__":
    cli()
