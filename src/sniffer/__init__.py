import os
import argparse

import zmq
import yaml

import slp


parser = argparse.ArgumentParser()
parser.add_argument("--type", default="raw")
parser.add_argument("--fecf", action="store_true")
args = parser.parse_args()

# load main config file
config_file = os.environ.get("CONFIG_FILE", "config.yml")
if not os.path.exists(config_file):
    raise ValueError(f"The file {config_file} was not found.")
with open(config_file, 'r') as f:
    config = yaml.safe_load(f)

context = zmq.Context()
socket = context.socket(zmq.SUB)

if args.type == "raw":
    socket.setsockopt(zmq.SUBSCRIBE, b"")
elif args.type == "ocf":
    socket.setsockopt(zmq.SUBSCRIBE, b"onlt-ocf")
    socket.setsockopt(zmq.SUBSCRIBE, b"onlc-ocf")
    socket.setsockopt(zmq.SUBSCRIBE, b"offl-ocf")
elif args.type == "pkt":
    socket.setsockopt(zmq.SUBSCRIBE, b"onlt-pkt")
    socket.setsockopt(zmq.SUBSCRIBE, b"onlc-pkt")
    socket.setsockopt(zmq.SUBSCRIBE, b"offl-pkt")
socket.connect(f"tcp://localhost:{config['outgoing']['port']}")


def sniffer():

    try:
        while True:
            topic, data = socket.recv().split(b':', maxsplit=1)
            print(80*'-')
            if args.type == "raw":
                print(topic, ':')
                print(slp.hexdump(data, length=32))
            elif args.type == "ocf":
                print(topic, ':', slp.CommunicationsLinkControlWord.decode(data))
            elif args.type == "pkt":
                print(topic, ':', slp.SpacePacketPrimaryHeader.decode(data))

    except KeyboardInterrupt:
        socket.close()
        context.term()
