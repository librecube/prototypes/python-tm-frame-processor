import time
from return_frame_processor import ReturnFrameProcessor


def server():
    try:
        server = ReturnFrameProcessor()
        server.startup()
        print("Server started. Press Ctrl-C to stop.")
        while True:
            time.sleep(1)

    except KeyboardInterrupt:
        print("Shutting down...")
        server.shutdown()
