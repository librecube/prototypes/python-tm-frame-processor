import os
import queue

import yaml


class ReturnFrameProcessor:

    def __init__(self):
        self._queue = queue.Queue()

        # load main config file
        config_file = os.environ.get("CONFIG_FILE", "config.yml")
        if not os.path.exists(config_file):
            raise ValueError(f"The file {config_file} was not found.")
        with open(config_file, 'r') as f:
            config = yaml.safe_load(f)

        # controller
        from .controller import Controller
        self._controller = Controller(config['controller'])

        # outgoing data handler
        from .outgoing import Outgoing
        self._outgoing = Outgoing(
            config['outgoing'], self._controller, self._queue)

        # incoming data handler
        from .incoming import Incoming
        self._incoming = Incoming(
            config['incoming'], self._controller, self._queue)

    def startup(self):
        self._controller.startup()
        self._incoming.startup()
        self._outgoing.startup()

    def shutdown(self):
        self._incoming.shutdown()
        self._outgoing.shutdown()
        self._controller.shutdown()
