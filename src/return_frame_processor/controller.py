import threading
from xmlrpc.server import DocXMLRPCServer


class Controller:

    def __init__(self, config):
        self._config = config

        self._server = DocXMLRPCServer(
            ("0.0.0.0", self._config['port']), allow_none=True)
        self._server.register_introspection_functions()
        self._server.register_function(lambda: "pong", "ping")
        self._thread = threading.Thread(target=self._server.serve_forever)

    def startup(self):
        self._thread.start()

    def shutdown(self):
        self._server.shutdown()
        self._thread.join()

    def register_function(self, func, name=None):
        if name:
            self._server.register_function(func, name)
        else:
            self._server.register_function(func)
