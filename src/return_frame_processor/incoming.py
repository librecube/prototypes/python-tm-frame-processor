import zmq
import threading

import slp


class Incoming:

    def __init__(self, config, controller, queue):
        self._config = config
        self._controller = controller
        self._queue = queue

        self._context = zmq.Context()
        self._poller = zmq.Poller()
        self._clients = {}
        self._frame_buffer = {}

    def startup(self):
        for entry in self._config['channels']:
            zmq_socket = self._context.socket(zmq.SUB)
            zmq_socket.setsockopt(zmq.LINGER, 0)
            zmq_socket.connect(f"tcp://{entry['host']}:{entry['port']}")
            for topic in entry['topics']:
                topic = bytes(f"{topic}", 'utf-8')
                zmq_socket.setsockopt(zmq.SUBSCRIBE, topic)
            self._clients[entry['name']] = zmq_socket
            self._poller.register(zmq_socket, zmq.POLLIN)
        self._thread = threading.Thread(target=self._poll_incoming)
        self._thread.kill = False
        self._thread.start()

    def shutdown(self):
        self._thread.kill = True
        self._thread.join()
        for client in self._clients.values():
            client.close()
        self._context.term()

    def _queue_outgoing(self, topic, data):
        self._queue.put(b"%s:%s" % (topic, data))

    def _poll_incoming(self):
        thread = threading.currentThread()

        while not thread.kill:
            sockets = dict(self._poller.poll(1000))
            for socket, event in sockets.items():
                if event == zmq.POLLIN:
                    topic, data = socket.recv().split(b':', maxsplit=1)
                    self._process_incoming(topic, data)

    def _process_incoming(self, topic, data):
        # TODO: do error checking
        try:
            tmtf = slp.TelemetryTransferFrame.decode(
                data, self._config['fecf'])
        except slp.DecodingError:
            print("Could not decode received data, skipping...")
            return

        sc_id = tmtf.primary_header.spacecraft_id
        vc_id = tmtf.primary_header.vc
        vc_count = tmtf.primary_header.vc_frame_count

        topic_vc = bytes(f"-vc{vc_id}", 'utf-8')

        if tmtf.primary_header.flag_ocf:
            self._queue_outgoing(
                topic + b"-ocf" + topic_vc,
                tmtf.operational_control_field.encode())

        if tmtf.primary_header.flag_synch is True:
            # frame contains no space packet but custom data, forward as is
            self._queue_outgoing(
                topic + b"-custom" + topic_vc, tmtf.frame_data_field)
            return

        if (sc_id, vc_id) not in self._frame_buffer:
            if tmtf.primary_header.first_header_pointer ==\
                    slp.FirstHeaderPointer.ONLY_IDLE_DATA:
                # buffer empty, idle frame, skipping
                return
            elif tmtf.primary_header.first_header_pointer ==\
                    slp.FirstHeaderPointer.NO_PACKET_START:
                # buffer empty, no new packet starts in frame, skipping
                return
            else:
                self._frame_buffer[(sc_id, vc_id)] = {
                    'vc_count': vc_count,
                    'data': tmtf.frame_data_field[
                        tmtf.primary_header.first_header_pointer:]
                }
        else:
            expected_vc_count =\
                (self._frame_buffer[(sc_id, vc_id)]['vc_count'] + 1) & 0xff
            if vc_count != expected_vc_count:
                # one or more frames lost, clear buffer and start again
                del self._frame_buffer[(sc_id, vc_id)]
                self._process_incoming(topic, data)
                return
            else:
                self._frame_buffer[(sc_id, vc_id)]['vc_count'] = vc_count
                if tmtf.primary_header.first_header_pointer ==\
                        slp.FirstHeaderPointer.ONLY_IDLE_DATA:
                    # idle frame, skipping
                    return
                elif tmtf.primary_header.first_header_pointer ==\
                        slp.FirstHeaderPointer.NO_PACKET_START:
                    # append entire frame content to buffer
                    self._frame_buffer[(sc_id, vc_id)]['data'] +=\
                        tmtf.frame_data_field
                    return
                else:
                    # add remainder from previous frame to buffer
                    self._frame_buffer[(sc_id, vc_id)]['data'] +=\
                        tmtf.frame_data_field[
                            :tmtf.primary_header.first_header_pointer]
                    # extract remaining packets
                    self._extract_packets_from_buffer(
                        self._frame_buffer[(sc_id, vc_id)]['data'])
                    # start buffer new from first packet header
                    self._frame_buffer[(sc_id, vc_id)]['data'] =\
                        tmtf.frame_data_field[
                            tmtf.primary_header.first_header_pointer:]

        packets, remainder = self._extract_packets_from_buffer(
            self._frame_buffer[(sc_id, vc_id)]['data'])
        self._frame_buffer[(sc_id, vc_id)]['data'] = remainder
        for apid, packet in packets:
            topic_apid = bytes(f"-apid{apid:04}", 'utf-8')
            self._queue_outgoing(
                topic + b"-pkt" + topic_vc + topic_apid, packet)

    def _extract_packets_from_buffer(self, buffer):
        packets = []
        while True:
            try:
                space_packet_header =\
                    slp.SpacePacketPrimaryHeader.decode(buffer)
            except slp.DecodingError:
                return packets, buffer
            # remove decoded space packet from buffer
            packet = buffer[:space_packet_header.packet_len]
            buffer = buffer[space_packet_header.packet_len:]

            if space_packet_header.apid == slp.APID_IDLE_PACKET:
                # idle packet, ignoring
                pass
            else:
                packets.append((space_packet_header.apid, packet))
